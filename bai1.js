var ketQua = function () {
  var diemChuanvalue = document.getElementById("txt_diem_chuan").value * 1;
  var diemToanvalue = document.getElementById("txt_diem_toan").value * 1;
  var diemLyvalue = document.getElementById("txt_diem_ly").value * 1;
  var diemHoavalue = document.getElementById("txt_diem_hoa").value * 1;
  var khuVuc = document.getElementById("txt_khu_vuc").value * 1;
  var doiTuong = document.getElementById("txt_doi_tuong").value * 1;
  var diemTong = diemToanvalue + diemLyvalue + diemHoavalue + khuVuc + doiTuong;
  var checkDiemTong = diemTong >= diemChuanvalue;
  var checkDiemTungMon =
    diemToanvalue != 0 && diemLyvalue != 0 && diemHoavalue != 0;

  if (checkDiemTong && checkDiemTungMon) {
    document.getElementById(
      "ket_qua"
    ).innerHTML = `Bạn đã đậu. Tổng điểm: ${diemTong}`;
  } else if (checkDiemTong && !checkDiemTungMon) {
    document.getElementById(
      "ket_qua"
    ).innerHTML = `Bạn đã rớt. Tổng điểm: ${diemTong}`;
  } else {
    document.getElementById(
      "ket_qua"
    ).innerHTML = `Bạn đã rớt. Tổng điểm: ${diemTong}`;
  }
};
