var tinhTien = function () {
  var hoTen = document.getElementById("txt_ho_ten").value;
  var soKmvalue = document.getElementById("txt_so_km").value * 1;
  //   document.getElementById("ket_qua").innerText = `họ tên: ${hoTen}`;
  var so50kmDau = 50;
  var so50kmKe1 = 100 - so50kmDau;
  var so50kmKe2 = 200 - (so50kmDau + so50kmKe1);
  var so50kmKe3 = 350 - (so50kmDau + so50kmKe1 + so50kmKe2);
  var sokmConLai = soKmvalue - (so50kmDau + so50kmKe1 + so50kmKe2 + so50kmKe3);

  if (soKmvalue > 0 && soKmvalue <= 50) {
    var sotienphaitra = soKmvalue * 500;

    document.getElementById(
      "ket_qua"
    ).innerHTML = `Họ tên: ${hoTen}, Tiền điện: ${sotienphaitra}`;
  } else if (soKmvalue > 50 && soKmvalue <= 100) {
    var sotienphaitra = so50kmDau * 500 + so50kmKe1 * 650;
    document.getElementById(
      "ket_qua"
    ).innerHTML = `Họ tên: ${hoTen}, Tiền điện: ${sotienphaitra}`;
  } else if (soKmvalue > 100 && soKmvalue <= 200) {
    var sotienphaitra = so50kmDau * 500 + so50kmKe1 * 650 + so50kmKe2 * 850;
    document.getElementById(
      "ket_qua"
    ).innerHTML = `Họ tên: ${hoTen}, Tiền điện: ${sotienphaitra}`;
  } else if (soKmvalue > 200 && soKmvalue <= 350) {
    var sotienphaitra =
      so50kmDau * 500 + so50kmKe1 * 650 + so50kmKe2 * 850 + so50kmKe3 * 1100;
    document.getElementById(
      "ket_qua"
    ).innerHTML = `Họ tên: ${hoTen}, Tiền điện: ${sotienphaitra}`;
  } else {
    var sotienphaitra =
      so50kmDau * 500 +
      so50kmKe1 * 650 +
      so50kmKe2 * 850 +
      so50kmKe3 * 1100 +
      sokmConLai * 1300;
    document.getElementById(
      "ket_qua"
    ).innerHTML = `Họ tên: ${hoTen}, Tiền điện: ${sotienphaitra}`;
  }
};
